-- Fill CARD table
insert into CARD (number, balance, pin, status, attempts)
values ('4111111111111111', 100, '0001', 'blocked', 0);
insert into CARD (number, balance, pin, status, attempts)
values ('4111111111111112', 200, '0002', 'active', 0);
insert into CARD (number, balance, pin, status, attempts)
values ('4111111111111113', 300, '0003', 'active', 0);