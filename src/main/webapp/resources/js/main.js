/**
 * @author vkolodrevskiy
*/
$(document).ready(function() {
    $(".container .card-number-input input, .container .pin-input input, .container .withdraw-input input").keypress(function (e) {
            return false;
    });

    $(".container .withdraw > .key-block > .numbers input").on("click", function()
    {
        var input = $(".container .withdraw-input input");
        var inputVal = input.val();
        var appendex = $(this).attr("value");
        input.val(inputVal + appendex);
    });

    $(".container .login > .key-block > .numbers input").on("click", function()
    {
        var input = $(".container .pin-input input");
        var inputVal = input.val();
        if(inputVal.length < 4) {
            var appendex = $(this).attr("value");
            input.val(inputVal + appendex);
        }
    });

    $(".container .key-block > .numbers input").on("click", function()
    {
        var input = $(".container .card-number-input input");
        var inputVal = input.val();
        var inputLength = inputVal.replace(/\D/g, '').length;

        if(inputLength < 16) {
            var appendex = $(this).attr("value");
            if(inputLength%4==0 && inputLength!=0 && inputVal.slice(-1)!="-"){
                appendex = "-";
            }
            input.val(inputVal + appendex);
        }
    });

    $(".container .key-block .actions #clear").on("click", function()
    {
        $(".container .card-number-input input").val("");
        $(".container .pin-input input").val("");
        $(".container .withdraw-input input").val("");
    });

    $(".container .key-block .actions #sbmt-card").on("click", function()
    {
        var inputVal = $(".container .card-number-input input").val();
        if(inputVal.length < 19) {
           alert("Wrong card number.");
           return false;
        }
    });

    $(".container .key-block .actions #sbmt-pin").on("click", function()
    {
        var inputVal = $(".container .pin-input input").val();
        if(inputVal.length < 4) {
            alert("PIN format is wrong.");
            return false;
        }
    });
    $(".container .key-block .actions #sbmt-withdraw").on("click", function()
    {
        var inputVal = $(".container .withdraw-input input").val();
        if(inputVal.length < 1 || inputVal.slice(0, 1)=="0") {
            alert("Wrong amount.");
            return false;
        }
    });
});