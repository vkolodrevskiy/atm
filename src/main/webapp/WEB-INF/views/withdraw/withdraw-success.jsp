<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<%@ include file="../header.jsp" %>
<%@ include file="../top.jsp" %>
<div class="title">Operation was successful</div>
<div class="message">Your check</div>
<div class="cheque">
    <table>
        <tr>

            <th scope="col">CARD NUMBER</th>
            <th scope="col">DATE</th>
            <th scope="col">WITHDRAW AMOUNT</th>
            <th scope="col">BALANCE</th>
        </tr>
        <tr>
            <td>${number}</td>
            <td>${date}</td>
            <td>${amount}</td>
            <td>${balance}</td>
        </tr>
    </table>
</div>
<%@ include file="../footer.jsp" %>