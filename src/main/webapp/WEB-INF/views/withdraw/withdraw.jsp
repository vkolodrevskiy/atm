<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>
<%@ include file="../header.jsp" %>
<%@ include file="../top.jsp" %>
<div class="title">Withdraw</div>
<div class="message">Please enter amount to withdraw:</div>
<form:form action="/home/withdraw" modelAttribute="card">
    <div class="withdraw-input">
        <form:input path="balance" id="numberInput" readonly="true"></form:input>  </div>
    <form:errors path="balance"></form:errors>
  <div class="withdraw">  <%@ include file="../keyboard.jsp" %>
    <div class="actions">
        <input type="submit" value="OK" id="sbmt-withdraw"/>
        <input type="button" id="clear" value="CLEAR"/>
    </div>
    </div>
    </div>

</form:form>
<%@ include file="../footer.jsp" %>
