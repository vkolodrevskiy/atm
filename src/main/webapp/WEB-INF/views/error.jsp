<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ include file="header.jsp" %>
<%@ include file="top.jsp" %>
<div class="title">${title}</div>
<div class="error-message">${error}</div>

<%@ include file="footer.jsp" %>