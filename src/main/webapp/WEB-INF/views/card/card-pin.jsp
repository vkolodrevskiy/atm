<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page session="true" %>
<%@ include file="../header.jsp" %>
<%@ include file="../top.jsp" %>
<div class="title">Please enter your PIN</div>
<form method="post" class="signin" action="j_spring_security_check">
    <div class="pin-input">
        <input id="pinInput" name="j_password" type="password" readonly="readonly"/>
    </div>
    <input id="pin" name="j_username" type="hidden" value="${cardNumber}"/>
    <div class="login"><%@ include file="../keyboard.jsp" %>
        <div class="actions">
            <input type="submit" value="OK" id="sbmt-pin" name="commit"/>
            <input type="button" id="clear" value="CLEAR"/>
        </div>
    </div>
</form>
<%@ include file="../footer.jsp" %>
