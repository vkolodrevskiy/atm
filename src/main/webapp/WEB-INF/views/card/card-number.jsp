<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page session="true" %>
<%@ include file="../header.jsp" %>
<%@ include file="../top.jsp" %>
<div class="title">Please enter your card number</div>

<form:form action="/card/number" modelAttribute="card">
    <div class="card-number-input">
        <form:input path="number" id="numberInput" readonly="true"></form:input>  </div>
        <form:errors path="number"></form:errors>
        <%@ include file="../keyboard.jsp" %>
        <div class="actions">
            <input type="submit" value="OK" id="sbmt-card"/>
            <input type="button" id="clear" value="CLEAR"/>
        </div>
    </div>

</form:form>

<%@ include file="../footer.jsp" %>