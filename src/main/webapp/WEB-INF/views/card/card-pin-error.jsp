<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ include file="../header.jsp" %>
<%@ include file="../top.jsp" %>
<div class="title">Wrong PIN for this card</div>
<div class="error-message">Try again.</div>

<%@ include file="../footer.jsp" %>