<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ include file="../header.jsp" %>
<%@ include file="../top.jsp" %>
<div class="title">Card number issue</div>
<div class="error-message">Card number not found or blocked.</div>
<%@ include file="../footer.jsp" %>