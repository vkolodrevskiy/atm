<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ include file="header.jsp" %>
<%@ include file="top.jsp" %>
<div class="title">Balance</div>
<div class="balance-message">${date}</div>
<div class="balance-message">Card #: ${number}</div>
<div class="balance-message">Amount on card: ${balance}</div>
<%@ include file="footer.jsp" %>