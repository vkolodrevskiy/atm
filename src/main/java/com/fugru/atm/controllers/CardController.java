package com.fugru.atm.controllers;

import com.fugru.atm.dao.domain.Card;
import com.fugru.atm.service.CardService;
import javassist.tools.rmi.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Card management controller.
 * Handles entering card number and login to system with pin code.
 *
 * @author vkolodrevskiy
 */
@Controller
@RequestMapping("/card")
public class CardController {
    @Autowired
    private CardService cardService;

    private static final String CARD_NUMBER = "cardNumber";

    @RequestMapping(value = "number", method=RequestMethod.GET)
    public String cardGet(Model model) {
        model.addAttribute("card", new Card());
        return "/card/card-number";
    }

    @RequestMapping(value = "number", method=RequestMethod.POST)
    public String cardPost(Card card, BindingResult result,
                           HttpServletRequest request) throws ObjectNotFoundException {
        if(result.hasErrors()) {
            return "redirect:/card/number";
        }
        String number = card.getNumber().replace("-","");

        if(!cardService.сardAvailable(number)) {
            return "/card/card-number-error";
        }

        request.getSession().setAttribute(CARD_NUMBER, number);
        return "redirect:/card/pin";
    }

    @RequestMapping(value = "pin", method=RequestMethod.GET)
    public String pinGet(Model model, HttpServletRequest request) throws ObjectNotFoundException {
        String cardNumber = getCardNumberFromSession(request);

        if(cardService.loginAttemptsEnded(cardNumber)) {
            return "/card/card-pin-blocked";
        }
        model.addAttribute("cardNumber", cardNumber);

        return "/card/card-pin";
    }

    @RequestMapping(value = "pin/error", method=RequestMethod.GET)
    public String invalidPin(HttpServletRequest request) throws ObjectNotFoundException {
        String cardNumber = getCardNumberFromSession(request);

        if(cardService.loginAttemptsEnded(cardNumber)) {
            cardService.blockCard(cardNumber);
            return "/card/card-pin-blocked";
        }

        return "/card/card-pin-error";
    }

    private String getCardNumberFromSession(HttpServletRequest request) {
        return (String) request.getSession().getAttribute(CARD_NUMBER);
    }
}
