package com.fugru.atm.controllers;

import com.fugru.atm.dao.domain.Card;
import com.fugru.atm.enums.OperationCode;
import com.fugru.atm.service.CardService;
import javassist.tools.rmi.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Date;

/**
 * Controller for operations
 *
 * @author vkolodrevskiy
 */
@RequestMapping("/home")
@Controller
public class HomeController {
    @Autowired
    private CardService cardService;

    private static final String CARD_AMOUNT_FLASH = "cardAmount";

    @RequestMapping(method=RequestMethod.GET)
    public String homeGet() {
        return "home";
    }

    @RequestMapping(value="balance", method=RequestMethod.GET)
    public String balanceGet(Model model) throws ObjectNotFoundException {
        String number = getLogedInCardNumber();
        Double balance = cardService.cardBalance(number);
        Date date = new Date();
        cardService.logBalance(number, date, OperationCode.balance.toString());
        model.addAttribute("date", date);
        model.addAttribute("balance", balance);
        model.addAttribute("number", number);
        return "balance";
    }

    @RequestMapping(value="withdraw", method=RequestMethod.GET)
    public String withdrawGet(Model model) {
        model.addAttribute("card", new Card());
        return "/withdraw/withdraw";
    }

    @RequestMapping(value="withdraw", method=RequestMethod.POST)
    public String withdrawPost(Card card, RedirectAttributes redirectAttrs) throws ObjectNotFoundException {
        Double amount = card.getBalance();
        String number = getLogedInCardNumber();
        if(cardService.withdraw(number, amount)){
            cardService.logWithdraw(number, OperationCode.withdraw.toString(), amount);
            redirectAttrs.addFlashAttribute(CARD_AMOUNT_FLASH, amount);
            return "redirect:withdraw/success";
        }

        return "redirect:withdraw/error";
    }

    @RequestMapping(value="withdraw/success", method=RequestMethod.GET)
    public String withdrawSuccessGet(Model model,  @ModelAttribute(CARD_AMOUNT_FLASH) Double amount) throws ObjectNotFoundException {
        String number = getLogedInCardNumber();
        Double balance = cardService.cardBalance(number);
        Date date = new Date();
        model.addAttribute("date", date);
        model.addAttribute("balance", balance);
        model.addAttribute("number", number);
        model.addAttribute("amount", amount);
        return "/withdraw/withdraw-success";
    }
    @RequestMapping(value="withdraw/error", method=RequestMethod.GET)
    public String withdrawErrorGet(Model model) {
        model.addAttribute("title", "Withdraw error");
        model.addAttribute("error", "Not enough funds on card");
        return "error";
    }

    private String getLogedInCardNumber() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getName();
    }

    public void setProductManager(CardService cardService) {
        this.cardService = cardService;
    }
}
