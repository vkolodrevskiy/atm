package com.fugru.atm.dao;

import com.fugru.atm.dao.domain.Card;
import javassist.tools.rmi.ObjectNotFoundException;

/**
 * DAO for card entity
 *
 * @author vkolodrevskiy
 */
public interface CardDao {
    Card findByNumberAndPin(String number, String pin) throws ObjectNotFoundException;
    Card findByNumber(String number);
    Byte getAttemptsNumByNumber(String number) throws ObjectNotFoundException;
    void incrementAttemptsByNumber(String number) throws ObjectNotFoundException;
    void flushAttemptsByNumber(String number) throws ObjectNotFoundException;
    Boolean withdrawByNumber(String number, Double amount) throws ObjectNotFoundException;
    Double getBalanceByNumber(String number) throws ObjectNotFoundException;
    void update(Card card);
}
