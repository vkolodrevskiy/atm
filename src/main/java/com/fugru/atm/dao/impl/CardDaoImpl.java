package com.fugru.atm.dao.impl;

import com.fugru.atm.dao.CardDao;
import com.fugru.atm.dao.domain.Card;
import com.fugru.atm.dao.domain.CardStatus;
import javassist.tools.rmi.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * DAO Methods implementation for Card Entity
 *
 * @author vkolodrevskiy
 */
@Repository
public class CardDaoImpl implements CardDao {
    @Autowired
    private HibernateTemplate hibernateTemplate;

    @Override
    public Card findByNumberAndPin(String number, String pin) throws ObjectNotFoundException {
        List<Card> result = getCardByNumberAndPin(number, pin);
        if(result.isEmpty()) throw new ObjectNotFoundException("Card with given info isn't found");
        return result.get(0);
    }

    @Override
    public Card findByNumber(String number) {
        List<Card> result = getCardByNumber(number);
        if(result.isEmpty()) return null;
        return result.get(0);
    }

    @Override
    public Byte getAttemptsNumByNumber(String number) throws ObjectNotFoundException {
        List<Card> result = getCardByNumber(number);
        if(result.isEmpty()) throw new ObjectNotFoundException("Card with given info isn't found");
        Card card = result.get(0);
        CardStatus cardStatus = card.getCardStatus();
        return cardStatus.getAttempts();
    }

    @Override
    public void incrementAttemptsByNumber(String number) throws ObjectNotFoundException {
        List<Card> result = getCardByNumber(number);
        if(!result.isEmpty()) {
            Card card = result.get(0);
            Byte attempts = card.getCardStatus().getAttempts();
            card.getCardStatus().setAttempts(++attempts);
            hibernateTemplate.update(card);
        } else throw new ObjectNotFoundException("Card with given info isn't found");
    }

    @Override
    public void flushAttemptsByNumber(String number) throws ObjectNotFoundException {
        List<Card> result = getCardByNumber(number);
        if(!result.isEmpty()) {
            Card card = result.get(0);
            card.getCardStatus().setAttempts((byte)0);
            hibernateTemplate.update(card);
        } else throw new ObjectNotFoundException("Card with given info isn't found");
    }

    @Override
    public Boolean withdrawByNumber(String number, Double amount) throws ObjectNotFoundException {
        List<Card> result = getCardByNumber(number);
        if(!result.isEmpty()) {
            Card card = result.get(0);
            Double balance = card.getBalance();
            if(balance - amount < 0) {
                return false;
            }
            card.setBalance(balance - amount);
            hibernateTemplate.update(card);
        } else throw new ObjectNotFoundException("Card with given info isn't found");
        return true;
    }

    @Override
    public Double getBalanceByNumber(String number) throws ObjectNotFoundException {
        List<Card> result = getCardByNumber(number);
        if(!result.isEmpty()) {
            Card card = result.get(0);
            return card.getBalance();
        } else throw new ObjectNotFoundException("Card with given info isn't found");
    }

    @Override
    public void update(Card card) {
        hibernateTemplate.update(card);
    }

    private List<Card> getCardByNumber(String number) {
        return hibernateTemplate.findByNamedQuery("findCardByNumber", number);
    }

    private List<Card> getCardByNumberAndPin(String number, String pin) {
        return hibernateTemplate.findByNamedQuery("findCardByNumberAndPin", number, pin);
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
        this.hibernateTemplate = hibernateTemplate;
    }
}
