package com.fugru.atm.dao.impl;

import com.fugru.atm.dao.OperationsLogDao;
import com.fugru.atm.dao.domain.Card;
import com.fugru.atm.dao.domain.OperationsLog;
import javassist.tools.rmi.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * DAO methods implementation for operations logging
 *
 * @author vkolodrevskiy
 */
@Repository
public class OperationsLogDaoImpl implements OperationsLogDao {
    @Autowired
    private HibernateTemplate hibernateTemplate;

    @Override
    public OperationsLog save(OperationsLog operationsLog) {
        hibernateTemplate.save(operationsLog);
        return operationsLog;
    }

    @Override
    public void logWithdraw(String number, String code, Double amount) throws ObjectNotFoundException {
        List<Card> result = getCardByNumber(number);
        if(!result.isEmpty()) {
            Card card = result.get(0);
            OperationsLog operationsLog = new OperationsLog(card, new Date(), amount, code);
            hibernateTemplate.save(operationsLog);
        } else throw new ObjectNotFoundException("Card with given info isn't found");
    }

    @Override
    public void logBalance(String number, Date date, String code) throws ObjectNotFoundException {
        List<Card> result = getCardByNumber(number);
        if(!result.isEmpty()) {
            Card card = result.get(0);
            OperationsLog operationsLog = new OperationsLog(card, new Date(), code);
            hibernateTemplate.save(operationsLog);
        } else throw new ObjectNotFoundException("Card with given info isn't found");
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
        this.hibernateTemplate = hibernateTemplate;
    }

    private List<Card> getCardByNumber(String number) {
        return hibernateTemplate.findByNamedQuery("findCardByNumber", number);
    }
}
