package com.fugru.atm.dao.domain;

import com.fugru.atm.enums.CardStatusType;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

/**
 * Stores information for account status.
 *
 * @author vkolodrevskiy
 */
@Embeddable
public class CardStatus {
    @Enumerated(EnumType.STRING)
    private CardStatusType status;

    // tracks how many times person entered invalid pin
    @NotNull
    private Byte attempts;

    public CardStatusType getStatus() {
        return status;
    }

    public void setStatus(CardStatusType status) {
        this.status = status;
    }

    public Byte getAttempts() {
        return attempts;
    }

    public void setAttempts(Byte attempts) {
        this.attempts = attempts;
    }
}
