package com.fugru.atm.dao.domain;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Represents operation events for credit card.
 *
 * @author vkolodrevskiy
 */
@Entity
public class OperationsLog extends BaseEntity {
    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="CARD_ID")
    private Card card;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    private Double amount;

    @NotBlank
    private String code;

    public OperationsLog(Card card, Date date, Double amount, String code) {
        this.date = date;
        this.amount = amount;
        this.code = code;
        this.card = card;
    }

    public OperationsLog(Card card, Date date, String code) {
        this.date = date;
        this.code = code;
        this.card = card;
    }

    public OperationsLog() {}

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCode() { return code; }

    public void setCode(String code) { this.code = code; }
}
