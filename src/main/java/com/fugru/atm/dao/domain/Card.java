package com.fugru.atm.dao.domain;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Represents credit card.
 *
 * @author vkolodrevskiy
 */
@Entity
@NamedQueries({
        @NamedQuery(name="findCardByNumber",
                query="FROM Card WHERE number = ? "),
        @NamedQuery(name="findCardByNumberAndPin",
                query="FROM Card WHERE number = ? AND pin = ? ")})
public class Card extends BaseEntity {
    @Size(min = 16, max = 16)
    private String number;

    // TODO: migrate to BigDecimal, using Double for simplicity
    @NotNull
    private Double balance;

    @Size(min = 4, max = 4)
    private String pin;

    @Embedded
    private CardStatus cardStatus;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public CardStatus getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(CardStatus cardStatus) {
        this.cardStatus = cardStatus;
    }
}
