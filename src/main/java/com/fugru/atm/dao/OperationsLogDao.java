package com.fugru.atm.dao;

import com.fugru.atm.dao.domain.OperationsLog;
import javassist.tools.rmi.ObjectNotFoundException;

import java.util.Date;

/**
 * DAO for operations logging
 *
 * @author vkolodrevskiy
 */
public interface OperationsLogDao {
    OperationsLog save(OperationsLog operationsLog);
    void logWithdraw(String number, String code, Double amount) throws ObjectNotFoundException;
    void logBalance(String number, Date date, String code) throws ObjectNotFoundException;
}
