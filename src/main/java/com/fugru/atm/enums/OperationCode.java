package com.fugru.atm.enums;

/**
 * Represents operation codes
 * @author vkolodrevskiy
 */
public enum OperationCode {
    withdraw,
    balance
}
