package com.fugru.atm.enums;

/**
 * Represents credit card possible states.
 *
 * @author vkolodrevskiy
 */
public enum CardStatusType {
    active,
    blocked
}
