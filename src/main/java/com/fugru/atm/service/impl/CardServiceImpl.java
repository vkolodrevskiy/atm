package com.fugru.atm.service.impl;

import com.fugru.atm.dao.CardDao;
import com.fugru.atm.dao.OperationsLogDao;
import com.fugru.atm.dao.domain.Card;
import com.fugru.atm.enums.CardStatusType;
import com.fugru.atm.service.CardService;
import javassist.tools.rmi.ObjectNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * {@link CardService} implementation.
 *
 * @author vkolodrevskiy
 */
@Service
public class CardServiceImpl implements CardService {
    private final static Logger logger = LoggerFactory.getLogger(CardServiceImpl.class);
    private final static Integer MAX_ATTEMPTS = 4;

    @Autowired
    private CardDao cardDao;

    @Autowired
    private OperationsLogDao operationsLogDao;

    @Override
    public Boolean withdraw(String number, Double amount) throws ObjectNotFoundException {
        return cardDao.withdrawByNumber(number, amount);
    }

    @Override
    public Boolean сardAvailable(String number) throws ObjectNotFoundException {
        Card cardToCheck = cardDao.findByNumber(number);
        return cardToCheck != null && cardToCheck.getCardStatus().getStatus() != CardStatusType.blocked;
    }

    @Override
    public Boolean loginAttemptsEnded(String number) throws ObjectNotFoundException {
        return cardDao.getAttemptsNumByNumber(number) >= MAX_ATTEMPTS;
    }

    @Override
    public Double cardBalance(String number) throws ObjectNotFoundException {
        return cardDao.getBalanceByNumber(number);
    }

    @Override
    public void flushAttempts(String number) {
        Card card = cardDao.findByNumber(number);
        card.getCardStatus().setAttempts((byte) 0);
        cardDao.update(card);
    }

    @Override
    public void incrementAttempts(String number) {
        Card card = cardDao.findByNumber(number);
        card.getCardStatus().setAttempts((byte) (card.getCardStatus().getAttempts() + 1));
        cardDao.update(card);
    }

    @Override
    public void logWithdraw(String number, String code, Double amount) throws ObjectNotFoundException {
           operationsLogDao.logWithdraw(number, code, amount);
    }

    @Override
    public void logBalance(String number, Date date, String code) throws ObjectNotFoundException {
           operationsLogDao.logBalance(number, date, code);
    }

    @Override
    public void blockCard(String cardNumber) {
        Card card = cardDao.findByNumber(cardNumber);
        card.getCardStatus().setStatus(CardStatusType.blocked);
        cardDao.update(card);
        logger.info("Card blocked. Card number = {}", cardNumber);
    }

    public void setOperationsLogDao(OperationsLogDao operationsLogDao) {
        this.operationsLogDao = operationsLogDao;
    }
}
