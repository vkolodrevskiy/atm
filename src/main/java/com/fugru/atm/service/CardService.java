package com.fugru.atm.service;

import javassist.tools.rmi.ObjectNotFoundException;

import java.util.Date;

/**
 * Operation method for credit card.
 *
 * @author vkolodrevskiy
 */
public interface CardService {
    Boolean withdraw(String number, Double amount) throws ObjectNotFoundException;
    Boolean сardAvailable(String number) throws ObjectNotFoundException;
    Boolean loginAttemptsEnded(String number) throws ObjectNotFoundException;
    Double cardBalance(String number) throws ObjectNotFoundException;
    void blockCard(String cardNumer);

    void flushAttempts(String number);
    void incrementAttempts(String number);

    void logWithdraw(String number, String code, Double amount) throws ObjectNotFoundException;
    void logBalance(String number, Date date, String code) throws ObjectNotFoundException;
}
