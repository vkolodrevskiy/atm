package com.fugru.atm.service.security;

import com.fugru.atm.service.CardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;

/**
 * Increment number of login attempts for the given card.
 *
 * @author vkolodrevskiy
 */
@Component
public class AtmAuthFailListener implements ApplicationListener<AuthenticationFailureBadCredentialsEvent> {
    @Autowired
    private CardService cardService;
    private final static Logger logger = LoggerFactory.getLogger(AtmAuthFailListener.class);

    @Override
    public void onApplicationEvent(AuthenticationFailureBadCredentialsEvent event) {
        String cardNumber = (String) event.getAuthentication().getPrincipal();
        if(logger.isDebugEnabled()) {
            logger.debug("Incrementing number of attempts for card with number {}", cardNumber);
        }
        cardService.incrementAttempts(cardNumber);
    }
}
