package com.fugru.atm.service.security;

import com.fugru.atm.dao.CardDao;
import com.fugru.atm.dao.domain.Card;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Custom {@link UserDetailsService} implementation.
 *
 * @author vkolodrevskiy
 */
@Service
public class AtmUserDetailsService implements UserDetailsService {
    @Autowired
    private CardDao cardDao;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Card card = cardDao.findByNumber(s);

        if (card == null) {
            throw new UsernameNotFoundException("Card with the given card number not found.");
        }
        Collection<? extends GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        return new User(card.getNumber(), card.getPin(), authorities);
    }
}
