package com.fugru.atm.service.security;

import com.fugru.atm.service.CardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import java.security.Principal;

/**
 * After success login we should set number of login attempts for the given card number
 * to zero.
 *
 * @author vkolodrevskiy
 */
@Component
public class AtmAuthSuccessListener implements ApplicationListener<InteractiveAuthenticationSuccessEvent> {
    @Autowired
    private CardService cardService;
    private final static Logger logger = LoggerFactory.getLogger(AtmAuthSuccessListener.class);

    @Override
    public void onApplicationEvent(InteractiveAuthenticationSuccessEvent event) {
        String cardNumber = ((Principal)event.getSource()).getName();
        if(logger.isDebugEnabled()) {
            logger.debug("Flushing number of attempts for card with number {}", cardNumber);
        }
        cardService.flushAttempts(cardNumber);
    }
}
